import inspect


def patch_class(cls, method_name, function):
    old_method = getattr(cls, method_name)
    setattr(cls, '_old_%s' % method_name, old_method)
    setattr(cls, method_name, function)


def patch_base_serializer():  # pragma: no cover
    from rest_framework.serializers import BaseSerializer
    from multi_fetcher import MultiPrepareFetcher

    if hasattr(BaseSerializer, '_old_data'):
        return

    @property
    def data(self):
        if hasattr(self, 'initial_data') and not hasattr(self, '_validated_data'):  # noqa
            msg = (
                'When a serializer is passed a `data` keyword argument you '
                'must call `.is_valid()` before attempting to access the '
                'serialized `.data` representation.\n'
                'You should either call `.is_valid()` first, '
                'or access `.initial_data` instead.'
            )
            raise AssertionError(msg)

        if not hasattr(self, '_data'):
            if self.instance is not None and not getattr(self, '_errors', None):  # noqa
                self._data = MultiPrepareFetcher(
                    self.instance, self, getattr(self, 'context')).execute()
            elif hasattr(self, '_validated_data') and not getattr(self, '_errors', None):  # noqa
                self._data = self.to_representation(self.validated_data)
            else:
                self._data = self.get_initial()
        return self._data

    patch_class(BaseSerializer, 'data', data)


def patch_is_simple_callable():
    from rest_framework import fields

    def is_simple_callable(obj):  # pragma: no cover
        """
        True if the object is a callable that takes no arguments.
        """
        function = inspect.isfunction(obj)
        method = inspect.ismethod(obj)

        if not (function or method):
            return False

        # in case obj is decorator, we want to get the real one
        real_obj = getattr(obj, '__wrapped__', None)
        if real_obj:
            obj = real_obj

        args, _, _, defaults = inspect.getargspec(obj)
        len_args = len(args) if function else len(args) - 1
        len_defaults = len(defaults) if defaults else 0
        return len_args <= len_defaults

    fields.is_simple_callable = is_simple_callable
