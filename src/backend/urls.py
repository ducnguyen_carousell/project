from django.conf.urls import url, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    url(r'^v1/', include('toptal.apps.api.v1.urls')),
    url(r'^admin-api/', include('toptal.apps.api.admin.urls')),
    url(r'', include('toptal.apps.frontend.urls')),
]

urlpatterns += staticfiles_urlpatterns()
