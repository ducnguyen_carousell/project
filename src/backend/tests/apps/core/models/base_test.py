from google.appengine.ext import ndb

from toptal.apps.core.models import User

from tests.toptal_test import BaseToptalTest


class TestBaseModel(BaseToptalTest):

    def setUpModel(self):
        self.user = User.create(
            key=ndb.Key('User', 'kiennt'), name='kien')

    def test_create_success(self):
        self.assertEquals(1, User.query().count())

    def test_update(self):
        self.user.update(name='new kien')
        self.assertEquals('new kien', self.user.key.get().name)
