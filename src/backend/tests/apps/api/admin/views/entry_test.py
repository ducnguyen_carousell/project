import json

from google.appengine.ext import ndb

from toptal.libs import utils
from toptal.apps.api.admin.views import EntryViewSet
from toptal.apps.core.models import User, Entry, Report
from toptal.apps.core.services import EntryCreateService

from tests.toptal_test import ForceAuthenticateUserTest


class TestEntryViewSet(ForceAuthenticateUserTest):

    VIEW_CLASS = EntryViewSet

    def test_list_entry(self):
        for i in xrange(5):
            user = User.create(key=ndb.Key('User', 'test-%s' % i), name='test')
            Entry.create(
                user_key=user.key, distance=10,
                start_at=utils.now_delta(hours=-1),
                end_at=utils.now())

        res = self.client.get('/admin-api/entries?user_key=%s' % user.safe_id)
        self.assertEquals(200, res.status_code)
        data = json.loads(res.content)['data']
        self.assertEquals(1, len(data))

    def test_list_entry_with_filter(self):
        for i in xrange(5):
            Entry.create(
                user_key=self.user.key, distance=10,
                start_at=utils.now_delta(hours=-1),
                end_at=utils.now())

        res = self.client.get(
            '/admin-api/entries?start=%s' % (
                utils.now_delta(hours=1).strftime('%Y-%m-%d %H:%M:%S')))
        self.assertEquals(200, res.status_code)
        data = json.loads(res.content)['data']
        self.assertEquals(0, len(data))

    def test_get_entry(self):
        entry = Entry.create(
            user_key=self.user.key, distance=10,
            start_at=utils.now_delta(hours=-1),
            end_at=utils.now())

        res = self.client.get('/admin-api/entries/%s' % entry.safe_id)
        self.assertEquals(200, res.status_code)

    def test_create_entry(self):
        res = self.client.post('/admin-api/entries', data={
            'user_key': self.user.safe_id,
            'distance': 10,
            'start_at': utils.now().strftime('%Y-%m-%d %H:%M:%S'),
            'end_at': utils.now_delta(hours=2).strftime('%Y-%m-%d %H:%M:%S')
        })
        self.assertEquals(200, res.status_code)
        self.assertEquals(1, Entry.query().count())
        report = Report.query().get()
        self.assertEquals(10, report.total_distance)

    def test_update_entry(self):
        entry, report = EntryCreateService(
            self.user, user_key=self.user.key, distance=10,
            start_at=utils.now(),
            end_at=utils.now_delta(hours=2)).execute()
        res = self.client.put('/admin-api/entries/%s?selected_fields=%s' % (
            entry.safe_id, ','.join(['user.id', 'user.name'])), data={
                'distance': 20
        })
        self.assertEquals(200, res.status_code)
        self.assertEquals(1, Entry.query().count())
        self.assertEquals(20, int(report.key.get().total_distance))

    def test_delete_entry(self):
        entry, report = EntryCreateService(
            self.user, user_key=self.user.key, distance=10,
            start_at=utils.now(),
            end_at=utils.now_delta(hours=2)).execute()
        res = self.client.delete('/admin-api/entries/%s' % entry.safe_id)
        self.assertEquals(200, res.status_code)
        self.assertEquals(1, Entry.query().count())
        self.assertEquals(Entry.ENTRY_STATUS_DELETED, entry.key.get().status)
        self.assertEquals(0, int(report.key.get().total_distance))
