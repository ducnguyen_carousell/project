import os

from django.shortcuts import render
from version import VERSION


def index(request):
    debug = os.environ.get('SERVER_SOFTWARE', '').startswith('Dev')
    if debug:
        bundle_url = 'http://localhost:8080/assets'
    else:
        bundle_url = '/assets'
    return render(request, 'index.html', {
        'version': VERSION,
        'is_dev': debug,
        'bundle_url': bundle_url,
    })
