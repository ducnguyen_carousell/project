from google.appengine.ext import ndb

from toptal.libs import utils

from base import BaseModel


class AccessToken(BaseModel):
    """
    Access token has key is an UUID value
    """

    (
        ACCESS_TOKEN_STATUS_NORMAL,
        ACCESS_TOKEN_STATUS_DELETED
    ) = xrange(2)

    ACCESS_TOKEN_STATUS_CHOICES = (
        (ACCESS_TOKEN_STATUS_NORMAL, 'Status normal'),
        (ACCESS_TOKEN_STATUS_DELETED, 'Status deleted'),
    )

    user_key = ndb.KeyProperty(kind='User')
    expired_at = ndb.DateTimeProperty()
    refresh_token = ndb.StringProperty()
    refresh_expired_at = ndb.DateTimeProperty()
    status = ndb.IntegerProperty(default=ACCESS_TOKEN_STATUS_NORMAL)

    def is_valid(self):
        is_expired = utils.now() >= self.expired_at
        is_valid_status = self.status == self.ACCESS_TOKEN_STATUS_NORMAL
        return is_valid_status and not is_expired

    def is_valid_refresh(self):
        return self.refresh_expired_at > utils.now()
