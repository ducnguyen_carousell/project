from google.appengine.ext import ndb

from toptal.libs import utils
from toptal.apps.core.models import AccessToken


class UserSignupService(object):

    def __init__(self, username, name, password):
        self.username = username
        self.name = name
        self.password = password

    @ndb.tasklet
    def create_access_token(self, user):
        access_token = yield AccessToken.create_async(
            key=ndb.Key('AccessToken', utils.uuid()),
            user_key=user.key,
            expired_at=utils.now_delta(days=90),
            refresh_token=utils.uuid(),
            refresh_expired_at=utils.now_delta(days=120))
        raise ndb.Return(access_token)

    @ndb.tasklet
    def execute_async(self):
        from create import UserCreateService
        data = {
            'username': self.username,
            'name': self.name,
            'password': self.password
        }
        user = yield UserCreateService(**data).execute_async()
        access_token = yield self.create_access_token(user)
        raise ndb.Return(user, access_token)

    def execute(self):
        return self.execute_async().get_result()
