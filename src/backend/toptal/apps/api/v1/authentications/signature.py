import settings
import hmac
from hashlib import sha256

from rest_framework.authentication import BaseAuthentication

from toptal.exceptions import (
    InvalidAPIKeyError,
    InvalidSignatureError)


class SignatureAuthentication(BaseAuthentication):

    @classmethod
    def generate_sig(cls, endpoint, params, secret):
        sig = endpoint
        for key in sorted(params.keys()):
            sig += '|%s=%s' % (key, params[key])
        return hmac.new(secret, sig.encode('utf-8'), sha256).hexdigest()

    def authenticate(self, request):
        data = {}
        for k in request.query_params:
            data[k] = request.query_params[k]
        for k in request.data:
            data[k] = request.data[k]
        api_key = data.get('api_key')
        if api_key != settings.TOPTAL_API_KEY:
            raise InvalidAPIKeyError('invalid api key')

        api_secret = settings.TOPTAL_API_SECRET
        api_sig = data.pop('api_sig', None)
        expected_sig = self.generate_sig(request.path, data, api_secret)
        if api_sig != expected_sig:
            raise InvalidSignatureError('invalid api signature')
        return (None, None)
