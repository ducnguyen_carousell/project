from toptal.apps.api.v1.serializers import (
    EntrySerializer,
    EntryUpdateSerializer,
    EntryCreateSerializer)
from toptal.libs.rest_framework.filters import StartAtDateTimeFilter
from toptal.libs.rest_framework import handlers
from toptal.apps.core.models import Entry
from toptal.apps.core.services import (
    EntryCreateService,
    EntryUpdateService,
    EntryDeleteService)
from toptal.apps.api.v1.authentications import AccessTokenAuthentication

from base import ToptalBaseViewSet


class ListMixin(object):

    def list(self, request):
        queryset = Entry.query(
            Entry.user_key == request.user.key,
            Entry.status == Entry.ENTRY_STATUS_NORMAL)
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        s = EntrySerializer(
            page, many=True,
            context={'request': request, 'user': request.user})
        s.selected_fields = self.get_selected_fields(request)
        return self.get_paginated_response(s.data)


class CreateMixin(object):

    def create(self, request):
        s = EntryCreateSerializer(data=request.data)
        if s.is_valid():
            data = {k: v for k, v in s.validated_data.iteritems()}
            data['user_key'] = request.user.key
            entry, _ = EntryCreateService(
                request.user, **data).execute()
            s = EntrySerializer(
                entry, context={'request': request, 'user': request.user})
            s.selected_fields = self.get_selected_fields(request)
            return handlers.response_success(s.data)
        return handlers.response_validation_error(s.errors)


class RetrieveMixin(object):

    def retrieve(self, request, pk):
        entry = self.get_object(pk)
        s = EntrySerializer(
            entry, context={'request': request, 'user': request.user})
        s.selected_fields = self.get_selected_fields(request)
        return handlers.response_success(s.data)


class UpdateMixin(object):

    def update(self, request, pk):
        entry = self.get_object(pk)
        s = EntryUpdateSerializer(entry, data=request.data, partial=True)
        if s.is_valid():
            new_entry, _ = EntryUpdateService(
                request.user, entry, **s.validated_data).execute()
            s = EntrySerializer(
                new_entry, context={'request': request, 'user': request.user})
            s.selected_fields = self.get_selected_fields(request)
            return handlers.response_success(s.data)
        return handlers.response_validation_error(s.errors)


class DeleteMixin(object):

    def destroy(self, request, pk):
        entry = self.get_object(pk)
        new_entry, _ = EntryDeleteService(request.user, entry).execute()
        s = EntrySerializer(
            new_entry, context={'request': request, 'user': request.user})
        s.selected_fields = self.get_selected_fields(request)
        return handlers.response_success(s.data)


class EntryViewSet(
        ListMixin,
        CreateMixin,
        RetrieveMixin,
        UpdateMixin,
        DeleteMixin,
        ToptalBaseViewSet):

    authentication_classes = [AccessTokenAuthentication]
    model_class = Entry
    filter_backends = [StartAtDateTimeFilter]
