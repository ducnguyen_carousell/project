from toptal.libs.rest_framework.filters import StartAtDateFilter
from toptal.libs.rest_framework import handlers
from toptal.apps.core.models import Report
from toptal.apps.api.v1.serializers import ReportSerializer
from toptal.apps.api.v1.authentications import AccessTokenAuthentication

from base import ToptalBaseViewSet


class ListMixin(object):

    def list(self, request):
        queryset = Report.query(
            Report.user_key == request.user.key,
            Report.status == Report.REPORT_STATUS_NORMAL)
        queryset = self.filter_queryset(queryset)
        queryset.order(-Report.start_at)
        page = self.paginate_queryset(queryset)
        s = ReportSerializer(
            page, many=True,
            context={'request': request, 'user': request.user})
        s.selected_fields = self.get_selected_fields(request)
        return self.get_paginated_response(s.data)


class RetrieveMixin(object):

    def retrieve(self, request, pk):
        report = self.get_object(pk)
        s = ReportSerializer(
            report, context={'request': request, 'user': request.user})
        s.selected_fields = self.get_selected_fields(request)
        return handlers.response_success(s.data)


class ReportViewSet(
        ListMixin,
        RetrieveMixin,
        ToptalBaseViewSet):

    authentication_classes = [AccessTokenAuthentication]
    model_class = Report
    filter_backends = [StartAtDateFilter]
