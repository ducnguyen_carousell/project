from toptal.apps.api.admin.serializers import ReportSerializer
from toptal.libs.rest_framework import handlers
from toptal.libs.rest_framework.filters import (
    StartAtDateFilter,
    UserNameFilter,
    UserKeyFilter)
from toptal.apps.core.models import Report
from toptal.apps.api.admin.authentications import (
    AccessTokenAdminAuthentication)

from base import ToptalBaseAdminViewSet


class ListMixin(object):

    def list(self, request):
        queryset = Report.query(
            Report.status == Report.REPORT_STATUS_NORMAL)
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        s = ReportSerializer(
            page, many=True,
            context={'request': request, 'user': request.user})
        s.selected_fields = self.get_selected_fields(request)
        return self.get_paginated_response(s.data)


class RetrieveMixin(object):

    def retrieve(self, request, pk):
        report = self.get_object(pk)
        s = ReportSerializer(
            report, context={'request': request, 'user': request.user})
        s.selected_fields = self.get_selected_fields(request)
        return handlers.response_success(s.data)


class ReportViewSet(
        ListMixin,
        RetrieveMixin,
        ToptalBaseAdminViewSet):

    authentication_classes = [AccessTokenAdminAuthentication]
    model_class = Report
    filter_backends = [UserNameFilter, UserKeyFilter, StartAtDateFilter]
