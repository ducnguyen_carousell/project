class ToptalError(Exception):

    default_status_code = 402
    default_message = 'error'

    def __init__(self, message=None, status_code=None):
        self.detail_message = message
        self.detail_status_code = status_code

    @property
    def status_code(self):
        return self.detail_status_code or self.default_status_code

    @property
    def message(self):
        return self.detail_message or self.default_message


class ResourceNotFoundError(ToptalError):
    pass


class ResourceAlreadyExistError(ToptalError):
    pass


class AuthenticationFailedError(ToptalError):
    pass


class ExpiredRefreshTokenError(ToptalError):
    pass


class InvalidAPIKeyError(ToptalError):
    pass


class InvalidSignatureError(ToptalError):
    pass


class PermissionDeniedError(ToptalError):
    pass
