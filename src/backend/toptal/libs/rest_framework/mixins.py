from google.appengine.ext import ndb

from toptal.exceptions import ResourceNotFoundError


class DynamicMethodViewMixin(object):

    def dispatch(self, request, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        request = self.initialize_request(request, *args, **kwargs)
        self.request = request
        self.headers = self.default_response_headers  # deprecate?

        try:
            if request.method.lower() in self.http_method_names:
                handler = getattr(
                    self, request.method.lower(),
                    self.http_method_not_allowed)
                handler_kwargs = getattr(handler, 'kwargs', {})
                if handler_kwargs:
                    for key, value in handler_kwargs.iteritems():
                        setattr(self, key, value)
            else:  # pragma: no cover
                handler = self.http_method_not_allowed

            self.initial(request, *args, **kwargs)
            response = handler(request, *args, **kwargs)
        except Exception, exc:
            response = self.handle_exception(exc)

        self.response = self.finalize_response(
            request, response, *args, **kwargs)
        return self.response


class GetSelectedFieldMixin(object):

    def get_selected_fields(self, request, default_fields=None):
        raw_fields = request.query_params.get('selected_fields', '')
        if raw_fields:  # pragma: no cover
            raw_fields = raw_fields.split(',')
        else:
            raw_fields = default_fields or {}
        data = {}
        for field in raw_fields:
            nested_fields = field.split('.')
            value = data
            for field in nested_fields[:-1]:
                if field not in value:
                    value[field] = {}
                value = value[field]
            value[nested_fields[-1]] = True
        return data


class GetObjectMixin(object):

    def get_object(self, pk):
        try:
            result = ndb.Key(urlsafe=pk).get()
            if not result:
                raise ResourceNotFoundError(pk)
            return result
        except:
            pass
