from rest_framework.serializers import (
    BaseSerializer, Serializer, ListSerializer)
from rest_framework.fields import SkipField
from rest_framework.compat import OrderedDict
from google.appengine.ext import ndb


class MultiPrepareFetcher(object):  # pragma: no cover

    def __init__(self, instance, serializer, context=None):
        self.instance = instance
        self.serializer = serializer
        self.context = context

    @ndb.tasklet
    def _fetch_nested_serializer(self, field, instance, selected_fields):
        if isinstance(instance, ndb.Future):
            instance = yield instance

        if instance is None:
            raise ndb.Return(None)

        if selected_fields:
            value = selected_fields.get(field.field_name)
            if isinstance(value, dict):
                field.selected_fields = value or {'id': True}
            else:
                field.selected_fields = {'id': True}

        result = yield MultiPrepareFetcher(
            instance, field, self.context).execute_async()
        raise ndb.Return(result)

    @ndb.tasklet
    def _to_representation_field(self, field, data, selected_fields):
        try:
            attribute = field.get_attribute(self.instance)
            if callable(attribute):  # pragma: no cover
                if self.context:
                    attribute = attribute(**self.context)
                else:
                    raise RuntimeError(
                        '%s Unexpected attribute' % field.field_name)

            if isinstance(field, BaseSerializer):
                attribute = yield self._fetch_nested_serializer(
                    field, attribute, selected_fields)
            elif isinstance(attribute, ndb.Future):
                attribute = yield attribute

            if attribute is None:
                data[field.field_name] = None
            elif isinstance(field, BaseSerializer):
                data[field.field_name] = attribute
            else:
                value = field.to_representation(attribute)
                data[field.field_name] = value
        except SkipField:
            raise ndb.Return()

    @ndb.tasklet
    def _fetch_data_single_serializer(self):
        data = OrderedDict()
        selected_fields = getattr(self.serializer, 'selected_fields', {})
        yield [
            self._to_representation_field(field, data, selected_fields)
            for field in self.serializer.fields.values()
            if (not field.write_only and field.field_name in selected_fields)]

        raise ndb.Return(data)

    @ndb.tasklet
    def _fetch_data_list_serializer(self):
        if hasattr(self.serializer, 'selected_fields'):
            self.serializer.child.selected_fields = getattr(
                self.serializer, 'selected_fields')
        result = yield [
            MultiPrepareFetcher(
                item, self.serializer.child, self.context).execute_async()
            for item in self.instance]
        raise ndb.Return(result)

    def execute_async(self):
        if isinstance(self.serializer, ListSerializer):
            return self._fetch_data_list_serializer()
        elif isinstance(self.serializer, Serializer):
            return self._fetch_data_single_serializer()

    def execute(self):
        return self.execute_async().get_result()
