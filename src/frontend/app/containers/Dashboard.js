import React, { Component, PropTypes } from 'react';

class Dashboard extends Component {

  render() {
    return (
      <div className="alert alert-info clearfix">
        <h3 className="mt0">Toptal Application</h3>
        <p><strong>Welcome to toptal</strong></p>
      </div>
    );
  }
}

export default Dashboard;
