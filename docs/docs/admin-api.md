# Admin API documents for Toptal

## User sign in

POST `admin/accounts/signin`

Get parameters

+ `api_key`
+ `api_sig`

Post data

+ `username`
+ `password`

Error types

+ `InvalidAPIKeyError`
+ `InvalidSignatureError`
+ `ValidationError`

Return

[user object](./object/#user-attributes)


## Create User

POST `admin/users/`

Get parameters

+ `access_token`

Post data

[user object](./object/#user-attributes)

Error types

+ `InvalidAccessToken`
+ `PermissionDeniedError`

Return

[user object](./object/#user-attributes)


## Update User

PUT `admin/users/`

Get parameters

+ `access_token`

Post data

[user object](./object/#user-attributes)

Error types

+ `InvalidAccessToken`
+ `PermissionDeniedError`

Return

[user object](./object/#user-attributes)


## Search records

POST `admin/records/search`

Get parameters

+ `access_token`

Post data

[record](./object/#record-attributes) filter


Error types

+ `InvalidAccessToken`

Return

List of [record objects](./object/#record-attributes)
This API has pagination


## Create record

POST `admin/records`

Get parameters

+ `access_token`

Post data

[record object](./object/#record-attributes)

Error types

+ `InvalidAccessToken`

Return

[record object](./object/#record-attributes)


## Update record

PUT `admin/records/:record_id`

Get parameters

+ `access_token`

Post data

[record object](./object/#record-attributes)

Error types

+ `InvalidAccessToken`

Return

[record object](./object/#record-attributes)


## List all report

GET `admin/reports`

Get parameters

+ `access_token`

Error types

+ `InvalidAccessToken`

Return

List of [report objects](./object/#report-attributes)
This API has pagination


## Search report

POST `admin/reports/search`

Get parameters

+ `access_token`

Post data

[report](./object/#report-attributes) filter


Error types

+ `InvalidAccessToken`

Return

List of [report objects](./object/#report-attributes)
This API has pagination
